<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Text;
use AppBundle\Form\NewTextType;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class TextTranslatorController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET", "HEAD", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function TextsAction(Request $request)
    {
        $texts = $this->getDoctrine()
            ->getRepository('AppBundle:Text')
            ->findAll();

        $form = $this->createForm(NewTextType::class, null, [
            'method' => 'POST',
        ]);

        $text = new Text();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();

            $text->translate('ru')->setText($data['text']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($text);

            $text->mergeNewTranslations();

            $em->flush();

            return $this->redirectToRoute('app_texttranslator_texts');
        }


        return $this->render('@App/TextTranslator/new_text.html.twig', array(
            'form' => $form->createView(),
            'texts' => $texts
        ));
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/translate/{text_id}")
     * @param Request $request
     * @param int $text_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function translateAction(int $text_id, Request $request)
    {
        $languages = $this->getLanguagesForLable();

        $text = $this->getDoctrine()->getRepository('AppBundle:Text')->find($text_id);

        /** @var Translation[] $translations */
        $translations = $text->getTranslations();
        $builder = $this->createFormBuilder();
        $hasTranslate = [];
        $notTranslated = [];

        foreach ($translations as $translation){
            $hasTranslate[$translation->getLocale()] = true;
        }

        foreach($languages as $key => $language) {
            if(!isset($hasTranslate[$key])) {
                $builder->add($key, TextType::class, [
                    'required' => false,
                    'label' => $language
                ]);
                $notTranslated[] = $key;
            }
        }

        $builder->add('save', SubmitType::class, ['label' => 'Сохранить']);
        $form = $builder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $locales = array_keys($data);
            $locale_index = 0;

            foreach ($data as $datum){
                if ($datum != null){
                    $text->translate($locales[$locale_index])->setText($datum);

                }
                $locale_index++;
            }

            $text->mergeNewTranslations();

            $em->flush();
            return $this->redirectToRoute('app_texttranslator_translate', [
                'text_id' => $text_id
            ]);
        }
        $locales = array_keys($hasTranslate);

        return $this->render('AppBundle:TextTranslator:translate.html.twig', array(
            'form' => $form->createView(),
            'text' => $text,
            'locales' => $locales,
            'notTranslated' => $notTranslated
        ));
    }

    public function getLanguagesForLable(){
        return [
              'en' =>  'английский',
              'de' =>  'немецкий',
              'kg' =>  'кыргызский',
              'kz' =>  'казахский',
              'fr' =>  'французский'
        ];
    }

}
