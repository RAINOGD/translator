<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Text;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    public function load(ObjectManager $manager)
    {
        $text = new Text();
        $text->translate('ru')->setText('Слово');
        $text->translate('en')->setText('Word');
        $text->translate('kg')->setText('Соз');
        $manager->persist($text);

        $text1 = new Text();
        $text1->translate('ru')->setText('Дом');
        $text1->translate('en')->setText('Home');
        $text1->translate('kg')->setText('Уй');
        $text1->translate('kz')->setText('KZ home');
        $text1->translate('de')->setText('DE home');
        $text1->translate('fr')->setText('FR home');
        $manager->persist($text1);

        $text2 = new Text();
        $text2->translate('ru')->setText('Компьютер');
        $manager->persist($text2);
        $text->mergeNewTranslations();
        $text1->mergeNewTranslations();
        $text2->mergeNewTranslations();

        $tester = new User();
        $tester->setRoles(['ROLE_USER'])
            ->setEnabled(true)
            ->setUsername('Mr. Test')
            ->setEmail('tester@gmail.com');

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($tester, '123321');
        $tester->setPassword($password);

        $manager->persist($tester);
        $manager->flush();
    }
}
