<?php
namespace AppBundle\Features\Context;
use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_texttranslator_texts'));
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When /^вижу ссылку "([^"]*)" и кликаю на него$/
     * @param $arg1
     */
    public function вижуСсылкуИКликаюНаНего($arg1)
    {
        $this->clickLink($arg1);
    }

    /**
     * @When /^вижу форму авторизации и ввожу свои данные$/
     */
    public function вижуФормуАвторизацииИВвожуСвоиДанные()
    {
        $this->fillField('Имя пользователя', 'Mr. Test');
        $this->fillField('Пароль', '123321');
        $this->pressButton('Войти');
    }

    /**
     * @When /^я авторизуюсь, вводя данные в поля "([^"]*)" и "([^"]*)", затем нажимаю на кнопку "([^"]*)"$/
     * @param $arg1
     * @param $arg2
     * @param $arg3
     */
    public function яАвторизуюсьВводяДанныеВПоляИЗатемНажимаюНаКнопку($arg1, $arg2, $arg3)
    {
        $this->fillField($arg1, 'Mr. Test');
        $this->fillField($arg2, '123321');
        $this->pressButton($arg3);
    }

    /**
     * @When /^я добавляю новые слова в поле "([^"]*)" и нажимаю на кнопку "([^"]*)":$/
     * @param $arg1
     * @param $arg2
     * @param TableNode $table
     */
    public function яДобавляюНовыеСловаВПолеИНажимаюНаКнопку($arg1, $arg2, TableNode $table)
    {
        foreach ($table as $word){
            $this->fillField($arg1, $word['word']);
            $this->pressButton($arg2);
        }

        sleep(3);
    }

    /**
     * @When /^потом я захожу на страницу перевода каждого слова \- перевожу их, и завершаю:$/
     */
    public function потомЯЗахожуНаСтраницуПереводаКаждогоСловаПеревожуИхИЗавершаю(TableNode $table)
    {
        foreach ($table as $word){
            $this->clickLink($word['word']);
            $this->fillField('английский', 'На английском');
            $this->fillField('немецкий', 'На немецокм');
            $this->fillField('кыргызский', 'На кыргызском');
            $this->fillField('казахский', 'На казахском');
            $this->fillField('французский', 'На французском');
            sleep(2);
            $this->pressButton('Сохранить');
            sleep(2);
            $this->clickLink('Все тексты');
        }
    }


}


